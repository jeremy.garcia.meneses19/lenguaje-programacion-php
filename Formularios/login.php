<?php
    session_start();
    if(isset($_SESSION['ACTUAL'])){
        unset($_SESSION['ACTUAL']);
    }

    if(empty($_SESSION)){
        $_SESSION['USUARIO'] = [
            1 =>    [
                    'num_cta' => '1',
                    'nombre' => 'Admin',
                    'primer_apellido' => 'General',
                    'segundo_apellido' => '',
                    'contrasena' => 'adminpass123',
                    'genero' => 'O',
                    'fec_nac' => '25/01/1990'
                    ]
            ];
        
        }    
?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>LOGIN</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="css/login.css" rel="stylesheet" type="text/css">
</head>
<body>
	<form class="container" action="login.php?accion=get&texto=textoenget" method="POST">
    <h2 class="login_header">LOGIN</h2>
    <?php

        $bandera_login = false;

        if(!empty($_POST)){
		foreach($_SESSION['USUARIO'] as $llave => $valor){
				$info_usuario = $valor;
					if($_POST['num_cuenta'] == $valor['num_cta'] && $_POST['contrasena'] == $valor['contrasena']){
                        $bandera_login = true; 
                        break;                 					
					} 
		}

		if($bandera_login == true){
            $_SESSION['ACTUAL'] = $info_usuario;
                header('Location: info.php');
			    exit; 
				}else{
                    echo '<p>Usuario y/o contraseña incorrectos</p>';
                }	
        }

    ?>

            <input class="campo" type = "text" name="num_cuenta" placeholder="Número de cuenta"> 
            <input class="campo" type = "password" name="contrasena" placeholder="Contraseña">
            <input type = "submit" value = "Entrar" class="boton" >
    </form>
	

</body>
</html>
