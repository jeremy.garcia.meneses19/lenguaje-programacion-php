<?php
	session_start();
	if($_SESSION['ACTUAL'] == null){
		header('Location: login.php');
		exit; 
	}
?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Información</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="css/info.css" rel="stylesheet" type="text/css">
</head>
<body>

	<header> 
		<nav>
			<ul class = "menu">
				<li> <a href="info.php"><p class="home">Home </p></a> </li>
				<li> <a href="formulario.php"> Registrar Alumnos </a> </li>
				<li> <a href="login.php"> Cerrar Sesión </a> </li>
			</ul>
		</nav>
	</header>
	<?php
			echo '<h2>Usuario Autenticado</h2>';
			echo '<div class = "container">';
				echo '<div class = "info_header">';
				if(isset($_SESSION['ACTUAL']['imag'])){
					echo $_SESSION['ACTUAL']['imag'];
				}
					echo '<p class="autenticado">'. $_SESSION['ACTUAL']['nombre']. ' ' . $_SESSION['ACTUAL']['primer_apellido'] . '</p>';
				echo '</div>';
				
				foreach($_SESSION['ACTUAL'] as $llave_info => $valor_info){
					if($llave_info != 'contrasena' && $llave_info != 'imag'){
					echo '<div class = "info_div">';
						echo $llave_info . ' : ' . $valor_info;
					echo '</div>';		
					}
				}
				
			echo '</div>';

			echo '<h2>Datos Guardados</h2>';
			echo '<div class = "container">';
				echo '<div class = "info_column">';
				echo '<div class = "num_cuenta"> <b>#</b> </div> ';
				echo '<div class = "nombre"> <b>Nombre</b> </div>';
				echo '<div class = "primer_ap"> <b>Primer apellido</b> </div>';
				echo '<div class = "segundo_ap"> <b>Segundo apellido</b> </div>';
				echo '<div class = "genero"> <b>Genero</b> </div>';
				echo '<div class = "fecha_nac"> <b>Fecha Nacimiento</b> </div>';
				echo '</div>';
				foreach($_SESSION['USUARIO'] as $key => $val){
					echo '<div class = "info_column">';
					echo '<div class = "num_cuenta">'. $val['num_cta'] .'</div> ';
					echo '<div class = "nombre">'. $val['nombre'] .'</div>';
					echo '<div class = "primer_ap">'. $val['primer_apellido'] .'</div>';
					echo '<div class = "segundo_ap">'. $val['segundo_apellido'] .'</div>';
					echo '<div class = "genero">'. $val['genero'] .'</div>';
					echo '<div class = "fecha_nac">'. $val['fec_nac'] .'</div>';					
					echo '</div>';
				}
				

			echo '</div>';

	?>

</body>
</html>