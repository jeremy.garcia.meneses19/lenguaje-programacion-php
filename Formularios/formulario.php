<?php
	session_start();
	if($_SESSION['ACTUAL'] == null){
		header('Location: login.php');
		exit; 
	}
?>

<!doctype html>
<html>
<head>
<link href="css/formulario.css" rel="stylesheet" type="text/css">
<meta charset="UTF-8">
<title>Registro Alumno</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>
<body>

	<header> 
        <nav>
			<ul class = "menu">
				<li> <a href="info.php"> Home </a> </li>
				<li> <a href="formulario.php"> <p class="registrar">Registrar Alumnos</p> </a> </li>
				<li> <a href="login.php"> Cerrar Sesión </a> </li>
			</ul>
		</nav>
	</header>

    <h1>Registro de usuarios</h1>
	<form class="container" action="formulario.php?accion=get&texto=textoenget" method="POST">
				<div class="campo">
					<label class="label">Número de cuenta</label>
					<input class="input" name="num_cta" type="text" placeholder="Número de cuenta">
				</div>

				<div class="campo">
					<label class="label">Nombre</label>
					<input class="input" name="nombre" type="text" placeholder="JUAN">				
				</div>

				<div class="campo">
					<label class="label">Primer apellido</label>
					<input class="input" name="primer_apellido" type="text" placeholder="SANCHEZ">				
				</div>

				<div class="campo">
					<label class="label">Segundo apellido</label>
					<input class="input" name="segundo_apellido" type="text" placeholder="SANCHEZ">				
				</div>
				
				<!-- form radio control -->
				<div class="campo">
					<label class="label">Género</label>
					<label class="input"><input type="radio" name="genero" value="H" checked>H</label>
					<label class="input"><input type="radio" name="genero" value="M">M</label>
					<label class="input"><input type="radio" name="genero" value="O">O</label>	
					
				</div>			

				<div class="campo">
					<label class="label" class="form-label">Fecha de Nacimiento</label>
					<input class="input" name="fec_nac" type="date" placeholder="dd/mm/aaaa">
				</div>

				<div class="campo">
					<label class="label">Contraseña</label>
					<input class="input" name="contrasena" type="password" placeholder="Contraseña">
				</div>				

				<!-- Botones -->
				<div class="campo">
					<div class="label">
						<input type='submit' value="Registrar"/>
					</div>
				</div>
		</form>	

		<?php
			if(!empty($_POST)){
				
				$_SESSION['USUARIO'][$_POST['num_cta']]= [
					'num_cta' => $_POST['num_cta'],
					'nombre' => $_POST['nombre'],
					'primer_apellido' => $_POST['primer_apellido'],
					'segundo_apellido' => $_POST['segundo_apellido'],
					'contrasena' => $_POST['contrasena'],
					'genero' => $_POST['genero'],
					'fec_nac' => date_format(date_create($_POST['fec_nac']),'d/m/Y'),
					'imag' => '<img class="imag" src="https://i.pravatar.cc/80">'																															
				];
				header('Location: info.php');
				exit; 
			}
		?>

</body>
</html>