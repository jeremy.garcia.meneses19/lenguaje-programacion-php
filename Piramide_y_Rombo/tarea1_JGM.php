<html>
    <head>
        <title> Pirámide y Rombo Jeremy García</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    </head>

    <body>
        <center>
        <h1>Pirámide</h1>
        <?php
            $filas = 30;
            for ($i = 0; $i<= $filas; $i++){
                for($j=0; $j <= $i; $j++){
                    print '<i class="fas fa-apple-alt"></i>';
                }
                echo "<br>";
            }
        ?>
        </center>
        
        <br><br><br>
        <center>
        <h1>Rombo</h1>
        <?php
            $filas = 30;
            for ($i = 0; $i<= $filas-1; $i++){
                for($j=0; $j <= $i; $j++){
                    print '<i class="fas fa-apple-alt"></i>';
                }
                echo "<br>";
            }
            for ($i = $filas-1; $i> 0; $i--){
                for($j=$i; $j > 0; $j--){
                    print '<i class="fas fa-apple-alt"></i>';
                }
                echo "<br>";
            }            
        ?>
        </center>

    </body>

</html>