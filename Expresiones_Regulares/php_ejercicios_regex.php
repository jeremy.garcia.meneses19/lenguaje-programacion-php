<html>
<head>
    <title> Expresiones Regulares Jeremy García </title>
</head>

<header> <h1>Ejercicios expresiones regulares</h1> </header>

<body>

    <?php
            //García Meneses Jeremy.
            function validarPatron($valida){
            return $valida == 1 ? 'Válida' : 'No válida';
        }
        //Realizar una expresión regular que detecte emails correctos.
        echo '<h2>1. Correos electrónicos</h2><br>';

        echo '<p>Algunos ejemplos se tomaron de: <a href="https://en.wikipedia.org/wiki/Email_address#Examples"> Dirección de correo electrónico </a></p>';
        
        $patron = '/^([_a-z0-9-]+(\.[\+_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})?)$/i';
        
        //Ejemplos correctos
        $correo1 = 'very.common@hotmail.com';
        $correo2 = 'disposable.style.email.with+symbol@example.com'; 
        $correo3 = 'other.email-with-hyphen@example.com'; 
        $correo4 = 'admin@mailserver1';
        $correo5 = 'jose.lopez20@yahoo.com.mx'; 

        //Ejemplos incorrectos
        $correo6 = 'abcexample.com';
        $correo7 = 'A@b@c@example.com'; 
        $correo8 = 'a"b(c)d,e:f;g<h>i[j\k]l@example.com'; 
        $correo9 = 'this\ still\"not\\allowed@example.com';     

        $valida = preg_match($patron,$correo1);
        echo '<p>La cadena '. '<b>' .$correo1 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$correo2);
        echo '<p>La cadena '. '<b>' .$correo2 . '</b>' .' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$correo3);
        echo '<p>La cadena '. '<b>' .$correo3 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$correo4);
        echo '<p>La cadena '. '<b>' .$correo4 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$correo5);
        echo '<p>La cadena '. '<b>' .$correo5 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$correo6);
        echo '<p>La cadena '. '<b>' .$correo6 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$correo7);
        echo '<p>La cadena '. '<b>' .$correo7 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$correo8);
        echo '<p>La cadena '. '<b>' .$correo8 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$correo9);
        echo '<p>La cadena '. '<b>' .$correo9 . '</b>' . ' es: ' . validarPatron($valida). '</p>';        

        //Realizar una expresion regular que detecte Curps Correctos
        echo '<br><br><h2>2. CURP</h2><br>';
        echo '<p>Estructura tomada de: <a href="http://sistemas.uaeh.edu.mx/dce/admisiones/docs/guia_CURP.pdf"> Estructura CURP </a></p>';
        $curp1 = 'ABCD123456EFGHIJ78'; //No valido
        $curp2 = 'BALR971019MQRLNS09'; //Válido
        $curp3 = 'VAPL990124MQRZLC00'; //válido
        //FORMATO CURP: LETRA|VOCAL|LETRA|LETRA|AA|MM|DD|{H|M}|ENTIDAD DE NACIMIENTO|CONSONANTE|CONSONANTE|CONSONANTE|HOMOCLAVE
        $patron = '/^[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}([^1-9][0-9]|[^02-9][0-2])([^1-9][1-9]|[^02-9][0-9]|[^0-13-9][0-9]|[^0-35-9][0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$/i';
        $valida = preg_match($patron,$curp1);
        echo '<p>La cadena '. '<b>' .$curp1 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$curp2);
        echo '<p>La cadena '. '<b>' .$curp2 . '</b>' .' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$curp3);
        echo '<p>La cadena '. '<b>' .$curp3 . '</b>' .' es: ' . validarPatron($valida). '</p>';
        
        
        //Realizar una expresion regular que detecte palabras de longitud mayor a 50
        //formadas solo por letras.
        echo '<br><br><h2>3. Palabras con longitud mayor a 50</h2><br>';
        echo '<p>Ejemplos tomados de: <a href="https://revistadigital.inesem.es/idiomas/palabras-mas-largas-del-mundo/"> Las 5 palabras más largas del mundo </a></p>';        
        $palabra1 = 'Muvaffakiyetsezlestiricilestiriveremeyebileceklerimizdenmissinizcesine'; //válida
        $palabra2 = 'Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu'; //válida
        $palabra3 = 'electroencefalografista'; //No válida
        $patron = '/^([A-Za-z]){51,}$/i';
        $valida = preg_match($patron,$palabra1);
        echo '<p>La cadena '. '<b>' .$palabra1 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$palabra2);
        echo '<p>La cadena '. '<b>' .$palabra2 . '</b>' .' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$palabra3);
        echo '<p>La cadena '. '<b>' .$palabra3 . '</b>' .' es: ' . validarPatron($valida). '</p>';
        

        //Crea una funcion para escapar los simbolos especiales.
        //Simbolos especiales en expresiones regulares: . + * ? [ ^ ] $ ( ) { } = ! < > | : -
        //Lookahead (ver hacia adelante):
            //Positivo : se define como hola(?=chau)y va a matchear la palabra hola solo si a continuacion existe chau
            //Negativo : se define como hola(?!chau)y va a matchear la palabra hola solo si a continuacion NO existe chau
            //Lookbehind (ver hacia atras):
            //Positivo : se define como (?<=chau)holay va a matchear la palabra hola solo si a existe chau antes que hola
            //Negativo : se define como (?<!chau)holay va a matchear la palabra hola solo si NO existe chau antes que hola
        
        function escapar(&$cadena){

            $patrones = array(  '/((?<=.)\$|\$(?=.))|(\$(?!.)|(?<!.)\$)/',
                                '/((?<=.)\.|\.(?=.))|(\.(?!.)|(?<!.)\.)/',
                                '/((?<=.)\+|\+(?=.))|(\+(?!.)|(?<!.)\+)/',
                                '/((?<=.)\*|\*(?=.))|(\*(?!.)|(?<!.)\*)/',
                                '/((?<=.)\?|\?(?=.))|(\?(?!.)|(?<!.)\?)/',
                                '/((?<=.)\[|\[(?=.))|(\[(?!.)|(?<!.)\[)/',
                                '/((?<=.)\^|\^(?=.))|(\^(?!.)|(?<!.)\^)/',
                                '/((?<=.)\]|\](?=.))|(\](?!.)|(?<!.)\])/',
                                '/((?<=.)\(|\((?=.))|(\((?!.)|(?<!.)\()/',
                                '/((?<=.)\)|\)(?=.))|(\)(?!.)|(?<!.)\))/',
                                '/((?<=.)\{|\{(?=.))|(\{(?!.)|(?<!.)\{)/',
                                '/((?<=.)\}|\}(?=.))|(\}(?!.)|(?<!.)\})/',
                                '/((?<=.)\=|\=(?=.))|(\=(?!.)|(?<!.)\=)/',
                                '/((?<=.)\!|\!(?=.))|(\!(?!.)|(?<!.)\!)/',
                                '/((?<=.)\<|\<(?=.))|(\<(?!.)|(?<!.)\<)/',
                                '/((?<=.)\>|\>(?=.))|(\>(?!.)|(?<!.)\>)/',
                                '/((?<=.)\||\|(?=.))|(\|(?!.)|(?<!.)\|)/',
                                '/((?<=.)\:|\:(?=.))|(\:(?!.)|(?<!.)\:)/',
                                '/((?<=.)\-|\-(?=.))|(\-(?!.)|(?<!.)\-)/');

            $sustituciones = array('\\\$',
                                    '\\\.',
                                    '\\+',
                                    '\\*',
                                    '\\?',
                                    '\\[',
                                    '\\^',
                                    '\\]',
                                    '\\(',
                                    '\\)',
                                    '\\{',
                                    '\\}',
                                    '\\=',
                                    '\\!',
                                    '\\<',
                                    '\\>',
                                    '\\|',
                                    '\\:',
                                    '\\-');

            $cadena = preg_replace($patrones,$sustituciones,$cadena);

        }
        
        echo '<br><br><h2>4. Escapar Simbolos especiales</h2><br>';
        $cadena1 = '$40 p|o.r $u!n $a g3*$400';
        $cadena2 = "h: < ola mundo (+php-) >{lenguaje}";

        echo '<p>Cadena original: <b>' .$cadena1 . '</b></p>';
        escapar($cadena1);
        echo '<p>Cadena escapada: <b>' .$cadena1 . '</b></p>';

        echo '<p>Cadena original: <b>' .$cadena2 . '</b></p>';
        escapar($cadena2);
        echo '<p>Cadena escapada: <b>' .$cadena2 . '</b></p>';



        //Crear una expresion regular para detectar números decimales.

        echo '<br><br><h2>5. Números decimales</h2><br>';
        $patron = '/^([-+]?[0-9]*.[0-9]+([Ee]([+-])?[0-9]+)?)$/i';
        $num1 = '-0.034';
        $num2 = '10e-3';
        $num3 = '1.4E3';
        $num4 = '.349';
        $num5 = '-1.5E+3';
        $valida = preg_match($patron,$num1);
        echo '<p>El número '. '<b>' .$num1 . '</b>' . ' es: ' . validarPatron($valida). '</p>';
        
        $valida = preg_match($patron,$num2);
        echo '<p>El número '. '<b>' .$num2 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$num3);
        echo '<p>El número '. '<b>' .$num3 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$num4);
        echo '<p>El número '. '<b>' .$num4 . '</b>' . ' es: ' . validarPatron($valida). '</p>';

        $valida = preg_match($patron,$num5);
        echo '<p>El número '. '<b>' .$num5 . '</b>' . ' es: ' . validarPatron($valida). '</p>';


    ?>

</body>


</html>